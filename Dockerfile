FROM python:latest
COPY . .
WORKDIR .
RUN pip install -r requirements.txt
EXPOSE 5000
RUN chmod +x start.sh
CMD ["sh", "-c", "./start.sh"]