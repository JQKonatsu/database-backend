# 数据管理技术 期中大作业实验报告

# DatabaseBackend说明文档

## 开发思路

> 根据作业要求，本后端项目应当实现三个功能
> 
> 1. 为数据库创建规定的表及触发器
> 
> 2. 从csv文件向数据库中注入数据
> 
> 3. 启动RESTful服务
> 
> 本项目使用ORM框架，使用sqlalchemy对数据库进行操作。使用flask创建应用，启动服务。

## 依赖说明

> Flask==2.3.1
> 
> Flask_RESTful==0.3.9
> 
> flask_sqlalchemy==3.0.3
> 
> SQLAlchemy==2.0.9
> 
> pymysql==1.0.3

## 部署准备

> * `pip install -r requirements.txt`安装依赖
> 
> * 在`src/`目录下创建`config.py`文件，格式如下（提交作业中已创建）
>   
>   ```
>   class BaseConfig(object):
>       HOST = 'xxx.xxx.xxx.xxx'
>       PORT = '3306'
>       DATABASE_NAME = 'database_name'
>       USERNAME = 'username'
>       PASSWORD = 'password'
>   
>       DATABASE_URI = "mysql+pymysql://{username}:{password}@{host}:{port}/{databasename}?charset=utf8mb4" \
>           .format(username=USERNAME, password=PASSWORD, host=HOST, port=PORT, databasename=DATABASE_NAME)
>   
>       SQLALCHEMY_DATABASE_URI = DATABASE_URI
>       SQLALCHEMY_TRACK_MODIFICATIONS = False
>   ```

## 使用说明

> 运行`create.sh`创建表和触发器
> 
> * 条件：数据库配置信息正确，且不存在将要创建的表和触发器
> 
> 运行`insert.sh`将根目录的csv文件数据注入数据库
> 
> * 条件：已完成第一项，已在根目录中添加csv文件，数据库的表中没有数据，本项目默认传入数据正确，符合表的定义
> 
> 运行`start.sh`启动RESTful服务
> 
> * 条件：已完成前两项

## 文件说明

> * `app.py`包含创建Flask应用的工厂方法`create_app()`和使用的应用实例`my_app`
> 
> * `create.py`包含创建表和触发器的方法，以及创建触发器的SQL原生代码
> 
> * `exts.py`包含数据库实例对象`db`
> 
> * `insert.py`实现从csv文件导入数据到数据库
> 
> * `models.py`包含所有ORM框架所需要的类来映射数据库当中的表
> 
> * `views.py`实现所有关于处理RESTful接口请求的视图函数
> 
> * `manage.py`处理运行python程序时的参数，来分别执行本项目的三个功能
> 
> * `config.py`储存关于链接数据库的配置信息

## 细节说明

> * 由于sqlalchemy的触发器实现是使用的监听函数，其实质上并不是数据库层面的触发器，故本项目使用SQL代码来创建触发器，同时不再在ORM框架层面实现监听函数。
> 
> * 因为数据库初始化需要应用实例，而应用启动又需要数据库实例，为避免可能的相互循环调用，创建`exts.py`来提供未初始化的`db = SQLAlchemy()`实例。
> 
> * 为保证代码目录级别的层次性，同时满足csv文件在根目录的要求，由于代码在`src/`目录中，csv文件在代码中使用的相对路径均以`../`开头。故在`.sh`文件中，需要先进入`src/`文件夹再执行python程序，来保证路径正确。

## 接口说明

### 数据插入

* `/api/v1/{table_name}`

* POST

* json

* `/api/v1/departments`

* ```json
  {
  "rows": [
      {"dept_no":"d009", "dept_name":"Customer Service"},
      {"dept_no":"d005", "dept_name":"Development"}
  ]
  }
  ```

### 数据更新

* `/api/v1/{table_name}`

* PUT

* json

* `/api/v1/departments`

* ```json
  {
  "dept_no":"d009", 
  "dept_name":"Customer Service"
  }
  ```

### 数据删除

* `/api/v1/{table_name}/{id}`

* DELETE

* none

* `/api/v1/titles/12000/Senior Engineer/1998-03-11`

### 数据查询

* `/api/v1/{table_name}/{id}`
  
  `/api/v1/{table_name}?xxx=yyy`

* GET

* none

* `/api/v1/dept_emp/10001/d009`
  
  `/api/v1/titles?title=Engineer`

## Docker部署

> 构建镜像`docker build -t database_backend .`
> 
> 运行镜像`docker run -p 5000:5000 database_backend`
