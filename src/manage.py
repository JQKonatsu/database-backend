from app import my_app
from create import create_table_and_trigger
from insert import insert_all
from views import load_views
import sys


def create():
    create_table_and_trigger()
    print("创建表和触发器成功")


def insert():
    insert_all()
    print("所有数据注入成功")


def start():
    load_views()
    my_app.run(debug=True)


if __name__ == '__main__':
    opt = sys.argv[1]
    if opt == 'create':
        create()
    elif opt == 'insert':
        insert()
    elif opt == 'start':
        start()
    else:
        print("参数错误")
